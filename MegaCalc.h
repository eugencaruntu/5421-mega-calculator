/*
Created by eugen on 6/28/2017
COMP 5421 :: Assignment 3
Eugen Caruntu #29077103
*/

#ifndef MEGACALC_H
#define LEMEGACALC_H
#include "MegaInt.h"

class MegaCalc
{
private:
	const string unaryCmds = "cnf!hq";
	const string binaryCmds = "+-*/%=";
	const string allCmds = "cnf!hq+-*/%=";
	MegaInt accumulator{ "0" };
	MegaInt operand{ "0" };
	string input;
	char command;

	bool cmdPresent = false;
	bool valid = false;

	/* factorial and hailstone functions */
	static MegaInt factorial(const MegaInt&);
	static MegaInt hailstone(const MegaInt&);

	// helper functions
	string removeSpaces(string& str);
	char findCommand(string& str);
	MegaInt findMagnitude(string& str);
	void MegaCalc::parse(string input);

public:
	MegaCalc();
	virtual ~MegaCalc();
	void run();
	// no more public member allowed here

};
#endif