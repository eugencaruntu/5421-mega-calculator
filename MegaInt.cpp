/*
Created by eugen on 6/28/2017
COMP 5421 :: Assignment 3
Eugen Caruntu #29077103
*/

#include "MegaInt.h"
#include <iostream>
#include <deque>
#include <ostream>
#include <algorithm>
#include <string>
#include <assert.h>

using std::string;
using std::deque;
using std::ostream;
using std::max;
using std::to_string;

// default ctor
MegaInt::MegaInt() :MegaInt{ "0" } // delegating to parameterized ctor
{
}

// parameterized/conversion ctor that would parse an input string
MegaInt::MegaInt(const string& str)
{
	for (const char& c : str) {
		mega_int.push_back(c);
	}

	// get the sign and remove it from deque
	if (!mega_int.empty() && mega_int.front() == '-') { sign = false; mega_int.pop_front(); } //negative
	if (!mega_int.empty() && mega_int.front() == '+') { sign = true; mega_int.pop_front(); } // positive
	if (mega_int.size() == 1 && mega_int.front() == '0') { sign = true; } //negative zero
}

// parameterized/conversion ctor that would parse an integer including the sign
MegaInt::MegaInt(const int& nbr) : MegaInt(to_string(nbr))
{
}

// copy ctor
MegaInt::MegaInt(const MegaInt& source)
{
	this->mega_int = source.mega_int;
	this->sign = source.sign;
}

// move ctor
MegaInt::MegaInt(MegaInt&& source) noexcept: MegaInt(source) // delegate to copy ctor
{
	source.mega_int.clear();
	source.sign = true;
}

// dtor
MegaInt::~MegaInt()
{
}

std::ostream& operator<<(std::ostream& sout, const MegaInt& source)
{
	char sgn = (source.sign == false) ? '-' : '+';
	sout << sgn;
	for (auto it = source.mega_int.begin(); it != source.mega_int.end(); it++)
	{
		sout << *it;
	}
	return sout;
}

MegaInt MegaInt::operator+() const
{
	return *this;
}

MegaInt MegaInt::operator-() const
{
	MegaInt N;
	N.mega_int = mega_int;
	N.sign = !sign;
	return N;
}

// overloaded prefix ++ operator
MegaInt MegaInt::operator++()
{
	MegaInt tmp = *this;
	*this = *this + MegaInt{ "1" };
	return tmp;
}

// overloaded postfix ++ operator
MegaInt MegaInt::operator++(int)
{
	MegaInt tmp = *this;
	*this = *this + MegaInt{ "1" };
	return tmp;
}

// overloaded prefix -- operator
MegaInt MegaInt::operator--()
{
	MegaInt tmp = *this;
	*this = *this - MegaInt{ "1" };
	return tmp;
}

// overloaded postfix -- operator
MegaInt MegaInt::operator--(int)
{
	MegaInt tmp = *this;
	*this = *this - MegaInt{ "1" };
	return tmp;
}

MegaInt& MegaInt::normalize()
{
	auto it = mega_int.begin();
	//if (*it == '-' || *it == '+') it++;		// jump the sign if is there (line might be scrapped if normalization happens after sign is removed)
	// check if at least first position is zero, if not do not proceed
	while (*it == '0' && it != mega_int.end() && mega_int.size() > 1)
	{
		it = mega_int.erase(it);	// erase will return next iterator
	}
	return *this;
}

deque<char> normalize(deque<char> d)
{
	auto it = d.begin();
	// check if at least first position is zero, if not do not proceed
	while (*it == '0' && it != d.end() && d.size() > 1)
	{
		it = d.erase(it);	// erase will return next iterator
	}
	return d;
}

int MegaInt::size() const
{
	return mega_int.size();
}

/* Clears the MegaInt and sets it to positive 0 */
MegaInt& MegaInt::c()
{
	mega_int.clear();
	mega_int.push_back('0');
	sign = true;
	return *this;
}

/* Negates the number */
MegaInt& MegaInt::n()
{
	sign = !sign;
	return *this;
}

bool MegaInt::getSign() const
{
	return sign;
}

/*Assignment operator overload
  Erases all elements in self, then  inserts  into  self  a
  copy of each element from right. Returns a reference to self.*/
const MegaInt& MegaInt::operator=(const MegaInt& rhs)
{
	if (&rhs != this)															// avoid self assignment
	{
		this->mega_int.clear();													// clear self
		for (auto it = rhs.mega_int.begin(); it != rhs.mega_int.end(); ++it)	// copy from right
		{
			this->mega_int.push_back(*it);
		}
		this->sign = rhs.sign;													// copy the sign
	}
	return *this;
}

/* Move assignment */
MegaInt& MegaInt::operator=(MegaInt&& rhs) noexcept
{
	if (this != &rhs) // direct check for self-assignment
	{
		this->mega_int.clear();													// clear self
		for (auto it = rhs.mega_int.begin(); it != rhs.mega_int.end(); ++it)	// copy from right
		{
			this->mega_int.push_back(*it);
		}
		this->sign = rhs.sign;													// copy the sign

		// leave rhs in a state in which it is safe to run the destructor
		rhs.mega_int.clear();
		rhs.sign = true;
	}
	return *this;
}

const int MegaInt::operator[](int i) const		// subscript operator overload: read only
{
	if (i < 0 || i >= mega_int.size())
	{
		throw string("MegaInt index is out of range at reading");
	}
	return mega_int[i] - '0';
}

char& MegaInt::operator[](int i)				// subscript operator overload: read and write
{
	if (i < 0 || i >= mega_int.size())
	{
		throw string("MegaInt index is out of range at writing");
	}
	return mega_int[i];
}

/* converting int to character */
char itc(const int i)
{
	if (!(0 <= i && i <= 9)) { throw string("Exception: the integer you try to convert is not one of 0-9 digits"); }
	return static_cast<char>(i + '0');
}

/* converting character to int */
int cti(const char ch)
{
	if (!('0' <= ch && ch <= '9')) { throw string("Exception: the character you try to convert is not one of 0-9 digits"); };
	return ch - '0';
}

/* overloading operator< as an adapted helper function for deque compare */
bool operator<(const deque<char>& lhs, const deque<char>& rhs)
{
	/* we now compare same signs */
	// fast check the lenght, if one is longer it will be decided imediatelly
	if (lhs.size() != rhs.size())				// we only process different lenghts here
	{
		return (lhs.size() < rhs.size());		// lhs < rhs with shorter lhs is true
	}	// if we are here then the size must be the same

		/* from here on we only have same size elements */
		// compare existing elements one by one starting at 0
	for (int i = 0; i < lhs.size(); i++)
	{
		if (lhs[i] != rhs[i])				// skip equal parts at begining
		{
			return (lhs[i] < rhs[i]);		// +lhs < +rhs
		}
	} return false;		// if we are here we must have gone through all positions of same lenght elements and they were all equal, so we return false
}

/* absolute value */
MegaInt MegaInt::abs() const
{
	MegaInt absVal{ *this };
	absVal.sign = true;
	return absVal;
}

/* helper function to concat with | as separator (used for div return)*/
deque<char> concat(const deque<char>& q, const deque<char>& r)
{
	deque<char> qr = q;
	qr.push_back('|');
	for (auto it = r.begin(); it != r.end(); ++it) {
		qr.push_back(*it);
	}
	return qr;
}

/* helper function to extract q or r | (used for / and % )*/
deque<char> extract(deque<char>& x, char c)
{
	// find position of separator and erase irrelevant part
	if (c == 'q')
	{
		x.erase(find(x.begin(), x.end(), '|'), x.end());
	}
	else if (c == 'r')
	{
		x.erase(x.begin(), ++find(x.begin(), x.end(), '|'));
	}

	// if c is neither q or r
	else { throw string("char argument must be 'q' or 'r'"); }

	return x;
}

MegaInt& MegaInt::operator+=(const MegaInt& rhs)
{
	MegaInt tmp = *this;
	*this = *this + rhs;
	return *this;
}

MegaInt& MegaInt::operator-=(const MegaInt& rhs)
{
	MegaInt tmp = *this;
	*this = *this - rhs;
	return *this;
}

MegaInt& MegaInt::operator*=(const MegaInt& rhs)
{
	MegaInt tmp = *this;
	*this = *this * rhs;
	return *this;
}

MegaInt& MegaInt::operator/=(const MegaInt& rhs)
{
	MegaInt tmp = *this;
	*this = *this / rhs;
	return *this;
}

MegaInt& MegaInt::operator%=(const MegaInt& rhs)
{
	MegaInt tmp = *this;
	*this = *this % rhs;
	return *this;
}

/* helper function for adition */
deque<char> plus(const deque<char>& X, const deque<char>& Y)
{
	int carry{ 0 };
	int m{ (int)X.size() };
	int n{ (int)Y.size() };
	int p{ max(m, n) + 1 };

	deque<char> Z(p, '0');				// to store the result of addition

	int i{ p - 1 };
	int j{ m - 1 };
	int k{ n - 1 };
	int t{ 0 };

	while (j >= 0 && k >= 0) {
		t = cti(X[j]) + cti(Y[k]) + carry;
		Z[i] = itc(t % 10);
		carry = t / 10;
		i--;
		j--;
		k--;
	}
	// Propagate last carry to unprocessed portion of X
	while (j >= 0) {
		t = cti(X[j]) + carry;
		Z[i] = itc(t % 10);
		carry = t / 10;
		i--;
		j--;
	}
	// Propagate last carry to unprocessed portion of Y
	while (k >= 0) {
		t = cti(Y[k]) + carry;
		Z[i] = itc(t % 10);
		carry = t / 10;
		i--;
		k--;
	}
	// Complete the operation
	Z[0] = itc(carry);
	return Z;
}

/* helper function for substraction */
deque<char> minus(const deque<char>& X, const deque<char>& Y)
{
	int borrow{ 0 };
	int m{ (int)X.size() };
	int n{ (int)Y.size() };
	int p{ max(m, n) };

	deque<char> Z(p, '0');

	int i{ p - 1 };
	int j{ m - 1 };
	int k{ n - 1 };
	int t{ 0 };

	while (j >= 0 && k >= 0)
	{
		t = cti(X[j]) - (cti(Y[k]) + borrow);
		borrow = 0;
		if (t < 0)
		{
			borrow = 1;
			t = 10 + t;
		}
		Z[i] = itc(t);
		i--;
		j--;
		k--;
	}
	// Propagate last borrow to unprocessed portion of X
	while (j >= 0)
	{
		t = cti(X[j]) - borrow;
		borrow = 0;
		if (t < 0)
		{
			borrow = 1;
			t = 10 + t;
		}
		Z[i] = itc(t);
		i--;
		j--;
	}
	if (borrow == 1 || k >= 0)
	{
		throw string("X cannot be less than Y in(X - Y)"); // Impossible! |X| < |Y | in (X - Y )
	}
	else
	{
		return Z;
	}
}

/* helper function for multiplication */
deque<char> mult(const deque<char>& X, const deque<char>& Y)
{
	int carry{ 0 };
	int n{ (int)X.size() };
	int m{ (int)Y.size() };
	int p{ max(n, m) };
	deque<char> partSum;
	int t{ 0 };

	for (int i = m - 1; i >= 0; i--)
	{
		carry = 0;
		deque<char> prod((m - i - 1), '0');	// make an empty deque to store each line multiplication
		for (int j = n - 1; j >= 0; j--)
		{	// multiply and push to pront of deque
			t = cti(X[j]) * cti(Y[i]) + carry;
			carry = t / 10;
			prod.push_front(itc(t % 10));
		}
		if (carry > 0) { prod.push_front(itc(carry)); } // push to front if carry is left with relevant value

		partSum = plus(partSum, prod);	// add to partial sum
	}
	return partSum;
}

/* helper function for division and modulo
   return quotient concatenated with remainder the result would have to be separated */
deque<char> div(const deque<char>& n, const deque<char>& d)
{
	deque<char> q;		// quotient
	deque<char> r;		// remainder
	deque<char> n1;		// n'
	deque<char> qk;		// quotient for each step
	deque<char> tmp;	// used to test how many times d fits in n'
	int s_n1;			// size of changing n'
	int s_dqk;			// size of d*qk 

	// take a subset from n of the same size as d
	n1.assign(n.begin(), n.end() - (n.size() - d.size()) - 1); // here we bring down one less than d size
	int i = n1.size();	// keep track of used positions

	do {
		n1.push_back(n[i]);		// then bring down one more position

		// compare n' with divisor and increase size of n' with one if needed
		n1 = normalize(n1);
		if (n1 < d  &&  n1.size() < d.size())	// to validate if the compare is accurate
		{
			qk.assign(1, '0');
		}
		else // we do not have compare for (n1 >= d), so we use a simple else
		{
			// test how many times qk*d fits in n'
			tmp.assign(1, '9'); // start at 9
			s_n1 = n1.size();	// get the new size of n'
			s_dqk = normalize(mult(d, tmp)).size();	// get the size of d*qk
			while (((n1 < normalize(mult(d, tmp))) && s_n1 == s_dqk) || s_n1 < s_dqk)
			{
				tmp.assign(1, itc(cti(tmp[0]) - 1));		// reduce tmp (guessed value) by 1
				s_dqk = normalize(mult(d, tmp)).size();		// update the size of d*qk
			}
			qk.assign(1, tmp[0]);							// use the value once it produces a fit
		}

		q.push_back(qk.back());								// update quotient result with qk

		r = normalize(minus(n1, normalize(mult(d, qk))));	// update remainder (we only keep track of last one)
		n1 = r;												// update n' with calculated remainder
		i++;												// update count of used positions
	} while (i < n.size());
	q = normalize(q);
	return concat(q, r);									// concatenate q + r using | as delimitor
}

MegaInt operator+(const MegaInt& lhs, const MegaInt& rhs)
{
	MegaInt R;
	if (lhs.sign == rhs.sign)
	{
		R.mega_int = plus(lhs.mega_int, rhs.mega_int);
		R.sign = lhs.sign; // override the sign with appropriate one
	}
	else
	{
		if (lhs.abs() > rhs.abs())
		{
			R.mega_int = minus(lhs.mega_int, rhs.mega_int);
			R.sign = lhs.sign; // override the sign with appropriate one
		}
		else if (lhs.abs() < rhs.abs())
		{
			R.mega_int = minus(rhs.mega_int, lhs.mega_int);
			R.sign = (!lhs.sign); // override the sign with appropriate one
		}
		else
		{
			R.c(); // clears and sets to positive 0
		}
	}
	R.normalize();
	return R;
}

MegaInt operator-(const MegaInt& lhs, const MegaInt& rhs)
{
	MegaInt R;
	if (lhs.sign != rhs.sign)
	{
		R.mega_int = plus(lhs.mega_int, rhs.mega_int);
		R.sign = lhs.sign; // override the sign with appropriate one
	}
	else
	{
		if (lhs.abs() > rhs.abs())
		{
			R.mega_int = minus(lhs.mega_int, rhs.mega_int);
			R.sign = lhs.sign; // override the sign with appropriate one
		}
		else if (lhs.abs() < rhs.abs())
		{
			R.mega_int = minus(rhs.mega_int, lhs.mega_int);
			R.sign = (!lhs.sign); // override the sign with appropriate one 
		}
		else
		{
			R.c(); // clears and sets to positive 0
		}
	}
	R.normalize();
	return R;
}

MegaInt operator*(const MegaInt& lhs, const MegaInt& rhs)
{
	MegaInt R;
	if (lhs.size() >= rhs.size())
	{
		R.mega_int = mult(lhs.mega_int, rhs.mega_int);	// (this is done disregarding the sign using only the deque)
	}
	else
	{
		R.mega_int = mult(rhs.mega_int, lhs.mega_int);	// (this is done disregarding the sign using only the deque)
	}

	if (lhs.sign != rhs.sign) { R.sign = false; }
	R.normalize();
	return R;
	if (R.size() == 1 && R[0] == '0') { R.sign = true; } // negative zero result is normalized
}

MegaInt operator/(const MegaInt& lhs, const MegaInt& rhs)
{
	MegaInt R;
	// check division by zero and throw
	if (rhs == MegaInt{ "0" } || rhs.size() == 0)
	{
		throw string("Division by zero exception. Try again.");
	}

	// check lhs < rhs and return 0
	if (lhs.size() < rhs.size() || lhs.size() == 0) { return R; } // R is zero here
	// check lhs == rhs and return 1
	if (lhs == rhs) { return MegaInt{ "1" }; }

	// else means lhs > rhs therefore we proceed with division and extract the quotient (this is done disregarding the sign using only the deque)
	R.mega_int = extract(div(lhs.mega_int, rhs.mega_int), 'q');

	// determine the sign
	if (lhs.sign != rhs.sign) { R.sign = false; }
	R.normalize();
	return R;
}

MegaInt operator%(const MegaInt& lhs, const MegaInt& rhs)
{
	MegaInt R;
	// check division by zero and throw
	if (rhs == MegaInt{ "0" } || rhs.size() == 0)
	{
		throw string("Division by zerooooo exception. Try again.");
	}

	// check lhs < rhs and return rhs
	if (lhs.size() < rhs.size() || lhs.size() == 0) { return rhs; }
	// check lhs == rhs and return 0
	if (lhs == rhs) { return R; } // R is zero here

	// else means lhs > rhs therefore we proceed with division and extract the remainder (this is done disregarding the sign using only the deque)
	R.mega_int = extract(div(lhs.mega_int, rhs.mega_int), 'r');

	/* If both operands are nonnegative then the remainder is nonnegative;
	 if not, the sign of the remainder is implementation-defined. */
	 // In this implementation, the sign is implemented as in division
	if (lhs.sign != rhs.sign) { R.sign = false; }
	R.normalize();
	if (R.size() == 1 && R[0] == '0') { R.sign = true; } // negative zero result is normalized

	return R;
}

bool operator==(const MegaInt & lhs, const MegaInt & rhs)
{
	if (lhs.sign != rhs.sign)				// check sign first
	{
		return false;
	}

	if (lhs.size() != rhs.size())			// check size
	{
		return false;
	}

	for (int i = 0; i < lhs.size(); ++i)	// compare each position
	{
		if (lhs[i] != rhs[i])				// using operator[]
		{
			return false;
		}
	}
	return true;
}

bool operator<(const MegaInt & lhs, const MegaInt & rhs)
{
	// check sign first
	if (lhs.sign != rhs.sign)
	{
		if (lhs.sign && !rhs.sign)return false;	// +lhs < -rhs is false
		if (!lhs.sign && rhs.sign)return true;	// -lhs < +rhs is true
	}	// if we are here then the signs must be the same

	/* we now compare same signs -lhs < -rhs and +lhs < +rhs */
	// fast check the lenght, if one is longer it will be decided imediatelly
	if (lhs.size() != rhs.size())										// we only process different lenghts here
	{
		if (lhs.sign && rhs.sign) return (lhs.size() < rhs.size());		// +lhs < +rhs with shorter lhs is true
		if (!lhs.sign && !rhs.sign) return !(lhs.size() < rhs.size());	// -lhs < -rhs with shorter lhs is false
	}	// if we are here then the size must be the same

	/* from here on we only have same size elements */
	// compare existing elements one by one starting at 0
	for (int i = 0; i < lhs.size(); i++)
	{
		if (lhs[i] != rhs[i])										// skip equal parts at begining
		{
			if (lhs.sign && rhs.sign) return (lhs[i] < rhs[i]);		// +lhs < +rhs
			if (!lhs.sign && !rhs.sign) return !(lhs[i] < rhs[i]);	// -lhs < -rhs
		}
	} return false;		// if we are here we must have gone through all positions of same lenght elements and they were all equal, so we return false
}

bool operator>(const MegaInt& lhs, const MegaInt& rhs)
{
	return  rhs < lhs;
}

bool operator!=(const MegaInt& lhs, const MegaInt& rhs)
{
	return !(lhs == rhs);
}

bool operator<=(const MegaInt& lhs, const MegaInt& rhs)
{
	return !(rhs < lhs);
}

bool operator>=(const MegaInt& lhs, const MegaInt& rhs)
{
	return !(lhs < rhs);
}
