/*
Created by eugen on 6/28/2017
COMP 5421 :: Assignment 3
Eugen Caruntu #29077103
*/

#ifndef MEGAINT_H
#define LEMEMEGAINT_H
#include <deque>
#include <iostream>

using std::string;
using std::deque;

class MegaInt
{
private:
	std::deque<char> mega_int{};
	bool sign{ true };						// positive by default
	
	/* helper functions */
	MegaInt abs() const;

public:
	MegaInt();								// defalt ctor
	explicit MegaInt(const std::string&);	// conversion ctor from string
	explicit MegaInt(const int& nbr);		// conversion ctor from int
	MegaInt(const MegaInt&);				// copy ctor
	MegaInt(MegaInt&& source) noexcept;		// move ctor
	virtual ~MegaInt();

	/* Assignment operator */
	const MegaInt& operator=(const MegaInt&);
	
	/* Move operator */
	MegaInt& operator=(MegaInt&& rhs) noexcept;

	/* Superscript operator */
	const int operator[](int i) const;			// subscript operator overload: read-only
	char& operator[](int i);					// subscript operator overload: read and write

	/* Arithmetic operators */
	friend MegaInt    operator+(const MegaInt& lhs, const MegaInt& rhs); // MegaInt + MegaInt
	friend MegaInt    operator-(const MegaInt& lhs, const MegaInt& rhs); // MegaInt - MegaInt
	friend MegaInt    operator*(const MegaInt& lhs, const MegaInt& rhs); // MegaInt * MegaInt
	friend MegaInt    operator/(const MegaInt& lhs, const MegaInt& rhs); // MegaInt / MegaInt
	friend MegaInt    operator%(const MegaInt& lhs, const MegaInt& rhs); // MegaInt % MegaInt
	MegaInt&          operator+=(const MegaInt& rhs); // MegaInt += MegaInt
	MegaInt&          operator-=(const MegaInt& rhs); // MegaInt -= MegaInt
	MegaInt&          operator*=(const MegaInt& rhs); // MegaInt *= MegaInt
	MegaInt&          operator/=(const MegaInt& rhs); // MegaInt /= MegaInt
	MegaInt&          operator%=(const MegaInt& rhs); // MegaInt %= MegaInt

	/* Relational operators */
	friend bool     operator==(const MegaInt& lhs, const MegaInt& rhs);	// MegaInt == MegaInt
	friend bool     operator<(const MegaInt& lhs, const MegaInt& rhs);	// MegaInt < MegaInt
	friend bool     operator>(const MegaInt& lhs, const MegaInt& rhs);	// MegaInt > MegaInt
	friend bool     operator!=(const MegaInt& lhs, const MegaInt& rhs); // MegaInt != MegaInt
	friend bool     operator<=(const MegaInt& lhs, const MegaInt& rhs); // MegaInt <= MegaInt
	friend bool     operator>=(const MegaInt& lhs, const MegaInt& rhs); // MegaInt >= MegaInt

	/* Unary operators */
	MegaInt operator+() const;	// unary +
	MegaInt operator-() const;	// unary -
	MegaInt operator++();		// unary prefix increment
	MegaInt operator--();		// unary prefix decrement
	MegaInt operator++(int);	// unary postfix increment
	MegaInt operator--(int);	// unary postfix decrement

	/* Operator << */
	friend std::ostream& operator<<(std::ostream& sout, const MegaInt& source);

	/* Clears the deque, used for c command */
	MegaInt& c();

	/* Negates the number */
	MegaInt& n();

	/* Normalizes the deque, (outside this class is being used for hailstone and factorial results) */
	MegaInt& normalize();

	/* accessors */
	int size() const;
	bool getSign() const;

};
#endif