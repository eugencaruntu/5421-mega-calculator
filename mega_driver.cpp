/*
Created by eugen on 6/28/2017
COMP 5421 :: Assignment 3
Eugen Caruntu #29077103
*/

#include "MegaCalc.h"
#include <exception>

using std::cout;
using std::endl;
using std::string;
using std::exception;

int main()
{
	try {
		MegaCalc mc;
		mc.run();
	}
	catch (exception& e) {
		cout << "Standard exception: " << e.what() << endl;
		exit(EXIT_FAILURE);
	}
	return EXIT_SUCCESS;
}