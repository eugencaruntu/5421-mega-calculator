COMP 5421 :: Assignment 3
Eugen Caruntu #29077103

DELIVERY NOTES / ASSUMPTIONS:

Exceptions:
	Exceptions are thrown and displayed where meaningful to user 
	They originate mostly from input parsing, operation restrictions (division by 0 for example) or casting.

Input parsing:
	Some atypical inputs are accepted and any assumption on them are always displayed to user, most notably:
		- the binary commands are accepted without rhs operand and accumulator is used instead (same as a regular calculator)
		- if unary command is found on first position the remainder of the input is ignored
		- extra +,- signs are ignored if found between operator and the sign of the number (the most right sign will be signing the number)
		- factorial can also be calculated using '!' as unary command in addition to 'n'

Performance:
	Due to the scope of this assignment, some calculations are using the MegaInt instead of a rather more efficient assertion.
	For example to determine if a MegaInt is even (in hailstone) we use the implemented modulo on MegaInt. 
		Performance would be improved if only last position in the deque is checked.
	Multiplication and division are implemented with the M2 and D2 algorithms.

The sign for modulo result:
	If both operands are nonnegative then the remainder is nonnegative;
	If not, the sign of the remainder is implementation-defined, in this case will follow the same logic as division)

Hailstone and factorial:
	The entire sequence is displayed on screen followed by pertinent result

<EOF>