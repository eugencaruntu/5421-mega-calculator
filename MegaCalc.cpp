/*
Created by eugen on 6/28/2017
COMP 5421 :: Assignment 3
Eugen Caruntu #29077103
*/

#include "MegaCalc.h"
#include <iostream>
#include <regex>

using std::regex;
using std::string;
using std::cin;
using std::getline;
using std::cout;
using std::endl;

MegaCalc::MegaCalc()
{
}

MegaCalc::~MegaCalc()
{
}

void MegaCalc::run()
{
	cout << "Welcome to MegaCalculator" << endl;
	cout << "=========================" << endl;
	do
	{
		cout << endl << "Accumulator: " << accumulator << endl;
		cout << "Enter input: ";

		getline(cin, input);		// read a command line

		if (input != "")
		{
			parse(input);			// parse the command line and modify Command object's members
		}							// otherwise if no input we do previous operation

		if (valid)					//EXECUTE THE COMMAND;
		{
			try {
				switch (command)
				{
					// binary commands
				case '+': accumulator = accumulator + operand;	break;
				case '-': accumulator = accumulator - operand;	break;
				case '*': accumulator = accumulator * operand;	break;
				case '/': accumulator = accumulator / operand;	break;
				case '%': accumulator = accumulator % operand;	break;
					// unary commands
				case '=': accumulator = operand;				break;
				case 'c': accumulator = accumulator.c();		break;
				case 'n': accumulator = accumulator.n();		break;
				case 'f': factorial(accumulator);				break;
				case '!': factorial(accumulator);				break;
				case 'h': hailstone(accumulator);				break;
				}
			}
			catch (string msg)
			{
				cout << msg << endl;
			}
		}

	} while (command != 'q');
}

/* parse the input */
void MegaCalc::parse(string input)
{
	cmdPresent = false;
	try
	{
		command = findCommand(input);			// find the command and extract it from input (therefore const function is not used)
	}
	catch (string msg)
	{
		cout << msg << endl;
	}

	if (cmdPresent)
	{
		try									// attempt to determine the magnitude from remainder of input
		{
			operand = findMagnitude(input);
		}
		catch (string msg)
		{
			cout << msg << endl;
		}
		return;
	}

	else if (!cmdPresent)
	{
		valid = false;
		return;
	}
}

/* Factorial */
MegaInt MegaCalc::factorial(const MegaInt& n)
{
	const MegaInt zero{ "0" };
	const MegaInt one{ "1" }; // similar to int int m{1}
	MegaInt mega_fact{ one }; // similar to int mega_fact {1}

	// test first if n is zero
	if (n == zero)
	{
		cout << n << "! = " << one << endl;
		return one;
	}
	// test if negative
	if (!n.getSign()) { throw string("Factorial for negative numbers is not defined"); }

	// do factorial
	for (MegaInt mega_k = one; mega_k <= n; ++mega_k)
	{
		mega_fact *= mega_k;
		cout << mega_k << "! = " << mega_fact << endl;
	}
	return mega_fact;
}

/* Hailstone */
MegaInt MegaCalc::hailstone(const MegaInt& n)
{
	MegaInt zero{ "0" };
	if (!n.getSign() || n == zero) { throw string("Why not trying a positive number greater than 0 instead to calculate hailstone :)"); }
	MegaInt mega_hail{ n };
	MegaInt one{ "1" };
	MegaInt two{ "2" };
	MegaInt three{ "3" };
	MegaInt cnt{ "1" };
	cout << "> " << mega_hail << endl;
	while (mega_hail != one)
	{
		if ((mega_hail % two) == zero)
		{
			mega_hail = mega_hail / two;
		}
		else
		{
			mega_hail = mega_hail * three + one;
		}
		cout << "> " << mega_hail << endl;
		cnt++;
	}
	cout << endl << "The length of the hailstone (" << n << ") sequence : " << cnt << endl;
	return cnt;
}

/* Extracting the command which would be on first position */
char MegaCalc::findCommand(string& str)
{
	str = removeSpaces(str);			// remove spaces

	if (str.size() == 0)
	{
		cmdPresent = true;
		return command;	// this will return previous command if input is empty (the sign and magnitude will remain the same)
	}

	else
	{
		char cmd = str.front();					// assume first character is the command
		int foundCmd = allCmds.find(cmd);		// if this is one of the commands
		if (foundCmd > -1)
		{
			str.erase(str.begin());				// remove first character if matches the command
			cmdPresent = true;
			return cmd;
		}
		else
		{
			cmdPresent = false;
			throw string("ERROR: The command was not recognized, please retry.");
		}
	}
}

/* Testing remainder of input once sign and command were extracted */
MegaInt MegaCalc::findMagnitude(string& str)
{
	int foundUnary = unaryCmds.find(command);
	int foundBinary = binaryCmds.find(command);

	if (str.size() == 0)	// nothing left in str to look for magnitude
	{
		if (foundUnary > -1)
		{
			valid = true;
			return MegaInt{ "0" };
		}
		else if (foundBinary > -1)
		{
			valid = true;
			operand = accumulator;
			throw string("NOTE: No magnitude in the input." + str + " Accumulator will be used as rhs operand for the \"" + command + "\" operation.");
		}
	}

	else if (str.size() > 0)	// try to determine if remainder of input is relevant and valid, then extract magnitude
	{
		if (foundUnary > -1)	// magnitude is not relevant, ignore input after unary command and notify user
		{
			valid = true;
			throw string("NOTE: the input \"" + str + "\" was ignored since it is not required after the unnary command \"" + command + "\".");
		}

		int foundBadChar = str.find_first_not_of("-+0123456789");
		if (foundBadChar > -1)
		{
			valid = false;
			throw string("ERROR: The input contains invalid characters (other than digits) \"" + str + "\".");
		}
		else if (foundBadChar == -1)
		{
			// if input is unsigned make it positive
			if (str.front() != '-' && str.front() != '+')
			{
				str = '+' + str;
			}
			// remove leading zeroes so we wont need normalization imediatelly after construction
			if (str.size() > 2)	// remove zero only for strings greater than 2 so we protect "+0" for example
			{
				str = regex_replace(str, regex("^(\\+|-)(0+)"), "$1");
				// if we are left with the sign only add a 0
				if (str.back() == '+' || str.back() == '-')
				{
					str = str + '0';
				}
			}
			// 'normalize' sign for zero (make it positive)
			if (str == "-0" || str == "0")
			{
				str = "+0";
			}
			//if successive signs are present use the first instance
			regex e("^([\\+-]+)([\\+-]\\d|[\\+-])");
			if (regex_search(str, e))
			{
				str = regex_replace(str, e, "$2");
				operand = MegaInt{ str };
				valid = true;
				throw string("NOTE: extra +/- signs were ignored. The number used is \"" + str + "\", and the comand is \"" + command + "\".");
			}

			// all good here
			valid = true;
			return MegaInt{ str };
		}
	}
}

/* Removing spaces from a string passed as argument */
string MegaCalc::removeSpaces(string& str)
{
	return regex_replace(str, regex("\\s+"), "");
}